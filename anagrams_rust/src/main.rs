use std::cmp::Ordering;
use std::ops::SubAssign;

#[derive(Eq, PartialEq, Clone)]
struct Letters([i8; 26]);
struct WordPair {
	word: String,
	letters: Letters,
}
#[derive(Copy, Clone)]
struct WordLinkedList<'a> {
	next: Option<&'a WordLinkedList<'a>>,
	word: &'a str,
}

fn prepend<'a>(next: Option<&'a WordLinkedList<'a>>, word: &'a str) -> WordLinkedList<'a> {
	WordLinkedList {
		next,
		word,
	}
}

fn print_words<'a>(mut words: Option<&'a WordLinkedList>) {
	while let Some(node) = words {
		words = node.next;
		if words.is_some() {
			print!("{} ", node.word);
		} else {
			println!("{}", node.word);
		}
	}
}

impl Letters {
	fn from_str(s: &str) -> Letters {
		let mut letters = Letters([0; 26]);
		s.bytes()
			.filter(u8::is_ascii_alphabetic)
			.for_each(|c| letters.0[(c - b'a') as usize] += 1);
		letters
	}

	fn empty(&self) -> bool {
		self.0.iter().all(|c| *c == 0)
	}
}

impl SubAssign<&Letters> for Letters {
	fn sub_assign(&mut self, rhs: &Letters) {
		for (l, r) in self.0.iter_mut().zip(rhs.0.iter()) {
			*l -= r;
		}
	}
}

impl PartialOrd<Letters> for Letters {
	fn lt(&self, other: &Letters) -> bool {
		self.0.iter().zip(other.0.iter()).all(|(s, o)| s < o)
	}

	fn le(&self, other: &Letters) -> bool {
		self.0.iter().zip(other.0.iter()).all(|(s, o)| s <= o)
	}

	fn gt(&self, other: &Letters) -> bool {
		self.0.iter().zip(other.0.iter()).all(|(s, o)| s > o)
	}

	fn ge(&self, other: &Letters) -> bool {
		self.0.iter().zip(other.0.iter()).all(|(s, o)| s >= o)
	}

	fn partial_cmp(&self, other: &Letters) -> Option<Ordering> {
		if self < other {
			Some(Ordering::Less)
		} else if self > other {
			Some(Ordering::Greater)
		} else if self == other {
			Some(Ordering::Equal)
		} else {
			None
		}
	}
}

//proc find_words(words: SinglyLinkedList[string], letters: Letters, word_list_start: int) =
//    if letters.empty_letters():
//        echo words.reversed().toSeq().join(" ")
//    else:
//        for i in word_list_start..word_list.high():
//            let word = word_list[i].word
//            let word_letters = word_list[i].letters
//            var letters = letters
//            var words = words
//            while letters >= word_letters:
//                letters -= word_letters
//                words.prepend(word)
//                find_words(words, letters, i+1)

fn find_words(words: Option<&'_ WordLinkedList<'_>>, letters: &Letters, word_list: &[WordPair]) {
	if letters.empty() {
		print_words(words);
	} else {
		for i in 0..word_list.len() {
			let WordPair {word, letters: word_letters} = &word_list[i];
			if !(letters >= word_letters) {
				continue;
			}
            let mut new_letters = letters.clone();
            new_letters -= word_letters;
            let word_node = prepend(words, word);
            let new_words = Some(&word_node);
            find_words(new_words, &new_letters, &word_list[i+1..]);
		}
	}
}

fn main() {
	let word_list = include_str!("../../res/wordlist.txt")
	    .lines()
	    .map(|word| WordPair{word: word.to_string(), letters: Letters::from_str(&word)})
	    .collect::<Vec<_>>();
    let phrase = std::env::args().skip(1).collect::<Vec<_>>().join(" ").to_lowercase();
	find_words(None, &Letters::from_str(&phrase), &word_list);
}
