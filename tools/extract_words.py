#!/usr/bin/env python

from collections import defaultdict
import argparse
import json
import re

parser = argparse.ArgumentParser(description='Extract words from json file(s)')
parser.add_argument(
	'files',
	metavar='file',
	type=argparse.FileType('r'),
	nargs='*',
	help='Json file to extract words from'
)
args = parser.parse_args()

def getDataStrings(data):
	if isinstance(data, list):
		for e in data:
			for string in getDataStrings(e):
				yield string
	elif isinstance(data, dict):
		for k, v in data.items():
			yield k
			for string in getDataStrings(v):
				yield string
	elif isinstance(data, str):
		yield data
	elif isinstance(data, bool):
		yield str(bool)

def getStrings():
	for f in args.files:
		data = json.load(f)
		for string in getDataStrings(data):
			yield string

def getWords():
	word_pattern = re.compile(r"\b[\w'\-\.&]+\b", re.ASCII)
	digit_pattern = re.compile(r"\d", re.ASCII)
	for string in getStrings():
		for m in word_pattern.finditer(string):
			word = m.group().lower()
			if not digit_pattern.search(word):
				yield word

seen_alpha_words = []
for word in sorted(set(getWords())):
	non_letter_pattern = re.compile(r"\W+")
	alpha_word = non_letter_pattern.sub('', word)
	junkShortWord = (
		len(alpha_word) == 1 and word not in ['a', 'i']
		or len(alpha_word) == 2 and word not in [
			'ad', 'ah', 'ai', 'am', 'an', 'as', 'at', 'be',
			'bi', 'by', 'dd', 'do', 'eh', 'ex', 'go', 'ha',
			'he', 'hi', 'ho', 'id', 'if', 'im', 'in', 'is',
			'it', 'ma', 'me', 'my', 'no', 'of', 'oh', 'on',
			'or', 'ow', 'ox', 'pa', 'pi', 'so', 'to', 'um',
			'up', 'us', 'we', 'yo',
		]
	)
	if junkShortWord or alpha_word in seen_alpha_words:
		continue
	seen_alpha_words.append(alpha_word)
	print(word)
