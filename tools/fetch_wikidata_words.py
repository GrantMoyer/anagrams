#!/usr/bin/env python

import argparse
import json
import re
import urllib.request
import urllib.parse

def fetch_words():
	wikidata_endpoint = 'https://query.wikidata.org/sparql'
	wikidata_query = '''
		SELECT DISTINCT ?r

		WHERE {
			?l a ontolex:LexicalEntry .
			?l dct:language wd:Q1860 . # English
			{?l wikibase:lexicalCategory wd:Q34698   .} UNION # adjective
			{?l wikibase:lexicalCategory wd:Q380057  .} UNION # adverb
			{?l wikibase:lexicalCategory wd:Q103184  .} UNION # article
			{?l wikibase:lexicalCategory wd:Q36484   .} UNION # conjunction
			{?l wikibase:lexicalCategory wd:Q126473  .} UNION # contraction
			{?l wikibase:lexicalCategory wd:Q576271  .} UNION # determiner
			{?l wikibase:lexicalCategory wd:Q83034   .} UNION # interjection
			{?l wikibase:lexicalCategory wd:Q1668170 .} UNION # interrogative adverb
			{?l wikibase:lexicalCategory wd:Q2304610 .} UNION # interrogative word
			{?l wikibase:lexicalCategory wd:Q1084    .} UNION # noun
			{?l wikibase:lexicalCategory wd:Q4833830 .} UNION # preposition
			{?l wikibase:lexicalCategory wd:Q36224   .} UNION # pronoun
			{?l wikibase:lexicalCategory wd:Q147276  .} UNION # proper noun
			{?l wikibase:lexicalCategory wd:Q24905   .} UNION # verb
			{
				?l wikibase:lexicalCategory wd:Q468801 . # personal pronoun
				# filter out neopronouns, because short words slow down the acronym search
			    FILTER NOT EXISTS {?l wdt:P31 wd:Q97441379 .}
			}
		    FILTER NOT EXISTS {?l wdt:P31 wd:Q101244 .} # filter out acronyms
		    FILTER NOT EXISTS {?l wdt:P31 wd:Q918270 .} # filter out initialisms
		    FILTER NOT EXISTS {?l wdt:P31 wd:Q102786 .} # filter out abbreviations
			?l ontolex:lexicalForm ?f .
		    FILTER NOT EXISTS {?f wikibase:grammaticalFeature wd:Q30619513 .} # filter out USPS abbreviations
			?f ontolex:representation ?r .
			FILTER(LANG(?r) IN ("en", "en-x-q7976")) .
		}
	'''

	query = urllib.parse.quote_plus(re.sub(r'(\s*#[^\n]*\n)?\s+', ' ', wikidata_query).strip())
	url = f'{wikidata_endpoint}?format=json&query={query}'
	with urllib.request.urlopen(url) as response:
		if response.status != 200:
			raise f'Failed to query wikidata: {response.status} {response.reason}'
		data = json.load(response)
	words = (b['r']['value'] for b in data['results']['bindings'])
	return [word.lower().strip() for word in words]

_ACCENT_REPLACEMENTS = {
	'a':  re.compile(r'[àáâãäåă]'),
	'ae': re.compile(r'[æ]'),
	'c':  re.compile(r'[ç]'),
	'd':  re.compile(r'[đ]'),
	'e':  re.compile(r'[èéêë]'),
	'i':  re.compile(r'[ìíïı]'),
	'n':  re.compile(r'[ñń]'),
	'o':  re.compile(r'[òóöōồớ]'),
	'oe': re.compile(r'[œ]'),
	'u':  re.compile(r'[üū]'),
	's':  re.compile(r'[şș]'),
	'z':  re.compile(r'[ź]'),
	'-':  re.compile(r'[–]'),
}
def remove_accents(word):
	for rep, pat in _ACCENT_REPLACEMENTS.items():
		pat.sub(rep, word)
	return word

_VALID_CHARS = re.compile(r'[a-z][a-z \'\-]*[a-z\']|[a-z]')
def valid_chars(word):
	return _VALID_CHARS.fullmatch(word)

_NON_LETTERS = re.compile(r'[^a-z]+')
def letters_only(word):
	return _NON_LETTERS.sub('', word)

def deduplicate_words(words):
	sorted_words = reversed(sorted(words, key=lambda word: (-len(letters_only(word)), -len(word), word)))
	delimiters = re.compile(r'[ \-]+')
	seen_parts = set()
	for word in sorted_words:
		parts = set(letters_only(part) for part in delimiters.split(word))
		if parts <= seen_parts:
			continue
		seen_parts |= parts
		yield word

_SHORT_WORDLIST = set([
	'a',  'i',  'we', 'us', 'up', 'to', 'so', 'ox', 'or', 'on',
	'of', 'no', 'my', 'me', 'it', 'is', 'in', 'if', 'id', 'he',
	'go', 'do', 'by', 'be', 'at', 'as', 'an', 'am', 'ah',
])
def filter_short_word(word):
	word_letters = letters_only(word)
	return len(word_letters) > 2 or word_letters in _SHORT_WORDLIST

def main(args):
	words = fetch_words()
	ascii_words = (word for word in map(remove_accents, words) if valid_chars(word))
	dedup_words = deduplicate_words(filter(filter_short_word, ascii_words))
	sorted_words = sorted(dedup_words, key=lambda word: (-len(letters_only(word)), word))
	for word in sorted_words:
		print(word)

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Fetch a word list from WikiData')
	args = parser.parse_args()
	main(args)
