import os
import std/lists
import std/sequtils
import std/strutils

type Letters = array['a'..'z', int8]
type WordPair = tuple[word: string, letters: Letters]

func reversed[T](l: SinglyLinkedList[T]): SinglyLinkedList[T] =
    for e in l:
        result.prepend(e)

func to_letters(phrase: string): Letters =
    for c in phrase:
        if c.isAlphaAscii():
            inc result[c]

const word_list = strutils
    .strip(staticRead("../res/wordlist.txt"))
    .splitLines()
    .map(proc(word: string): WordPair = (word: word, letters: word.to_letters))

func empty_letters(letters: Letters): bool =
    for count in letters:
        if count != 0:
            return false
    return true

func `-=`(a: var Letters, b: Letters) =
    for l in Letters.low()..Letters.high():
        a[l] -= b[l]

func `>=`(a: Letters, b: Letters): bool =
    for l in Letters.low()..Letters.high():
        if a[l] < b[l]:
            return false
    return true

proc find_words(words: SinglyLinkedList[string], letters: Letters, word_list_start: int) =
    if letters.empty_letters():
        echo words.reversed().toSeq().join(" ")
    else:
        for i in word_list_start..word_list.high():
            let word = word_list[i].word
            let word_letters = word_list[i].letters
            var letters = letters
            var words = words
            while letters >= word_letters:
                letters -= word_letters
                words.prepend(word)
                find_words(words, letters, i+1)

assert("" notin word_list.map(proc(e: WordPair): string = e.word))
assert(paramCount() >= 1, "Please provide phrase to anagram")
var phrase = commandLineParams().join(" ").toLower()
find_words(initSinglyLinkedList[string](), phrase.to_letters(), word_list.low())
